cmake_minimum_required(VERSION 2.8)

set (PROJECT testmingw)

set(TESTMINGW_VERSION 0.0.1)

project(${PROJECT})

set(SONAME ltestmingw)
set (EXNAME a)

add_subdirectory(lib)
add_subdirectory(bin)



