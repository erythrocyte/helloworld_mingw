#path to cmake
CMEXE="/c/Program Files/CMake/bin/cmake.exe"
#build type: Release, Debug, RelWithDebInfo, MinSizeRel
CMBT="Release"
#make program
CMMAKE="mingw32-make.exe"
#Install prefix
CMINSTALL_PATH="C:/pr/hellow"
 
"$CMEXE" -G "MSYS Makefiles" .. \
    -DCMAKE_BUILD_TYPE=$CMBT \
    -DCMAKE_MAKE_PROGRAM:PATH=$CMMAKE \
    -DCMAKE_INSTALL_PREFIX:PATH="$CMINSTALL_PATH"
#""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

