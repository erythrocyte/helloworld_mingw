#ifndef MYSPMAT_H
#define MYSPMAT_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <exception>

namespace AMGc{
	typedef double real;

	struct SpMatCSR {
		public:
			int   n;	    ///< Размерность матрицы.
			int  *row;	    ///< Указатели на начала строк.
			int  *col;	    ///< Столбцы ненулевых элементов.
			real *val;	    ///< Значения ненулевых элементов.
			bool  symmetric;    ///< Если установлен, то задается только нижняя 
						/// (верхняя) половина симметричной матрицы.

			/// Инициализация.
			SpMatCSR();

			/*
			* \param n	Размерность матрицы
			* \param nnz	Число ненулевых элементов.
			* \param I	Номера строк ненулевых элементов.
			* \param J	Номера столюцов ненулевых элементов.
			* \param V	Значения ненулевых элементов.
			*/
			SpMatCSR(int n, int nnz, const int *I, const int *J, const real *V);
	};

	/// Разреженная матрица с произвольным доступом к элементам.
	class SpMat {
		public:
		typedef std::map<int,real>::const_iterator row_iterator;

		/// Инициализация.
		/**
		* \param n Число строк матрицы.
		*/
		SpMat(int n);

		/// Деструктор.
		~SpMat();

		/// Число строк матрицы.
		int dim() const;

		/// Число ненулевых элементов матрицы.
		int nnz() const;

		/// Элемент матрицы.
		/**
		* \param i Строка.
		* \param j Столбец.
		* \return	Значение элемента.
		*/
		real  operator()(int i, int j) const;

		/// Элемент матрицы.
		/**
		* \param i Строка.
		* \param j Столбец.
		* \return	Значение элемента.
		*/
		real& operator()(int i, int j);

		/// Начало строки матрицы.
		/**
		* \param i Строка.
		* \return	Итератор по ненулевым элементам строки.
		*/
		row_iterator begin(int i) const;

		/// Конец строки матрицы.
		/**
		* \param i Строка.
		* \return	Конец строки.
		*/
		row_iterator end(int i) const;

		/// Формат CSR.
		SpMatCSR csr();

		/// Очистка матрицы.
		void clear();

		/// Сохранение в файл.
		void save();
		private:
		int size;			///< Число строк матрицы.
		std::map<int,real> *row;	///< Строки матрицы.

		bool  csr_uptodate;		///< Формат CSR соответствует текущему состоянию.
		int  *csr_row;		///< Указатели на начала строк в формате CSR.
		int  *csr_col;		///< Столбцы ненулевых элементов в формате CSR.
		real *csr_val;		///< Значения ненулевых элементов в формате CSR.
	};

	//struct SpMatVal{
		//int absIndex;
		//int i;
		//int j;
		//double val;
	//};

	//class SpMat{
		//private:
			//int fMtrDim;
		//public:
			//std::vector<SpMatVal> 
			//SpMat(int mtrDim);
			//~SpMat();
	//}



};

#endif
