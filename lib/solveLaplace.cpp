#include "solveLaplace.h"

void assembleMatrixDecart(AMGc::SpMat &smat, double *rhs)
{
	int n = smat.dim();
	double h = 1.0 / (n-1), h2 = h * h;
	smat(0,0) = 1.0; // left bound;
	smat(n-1, n-1) = 1.0; // right bound;
	rhs[0] = 0.0;
	rhs[n-1] = 1.0;
	for (int k = 1; k < n-1; k++){
		smat(k, k) = 2 / h2;
		smat(k, k-1) = - 1 / h2;
		smat(k, k+1) = - 1 / h2;
	}	
};

void assembleMatrixPolar(AMGc::SpMat &smat, double *rhs)
{
	int n = smat.dim();
	double *r = new double[n+1];
	r[0] = 0.01;
	r[n] = 1.0;
	double h = (r[n] - r[0]), h2 = h * h;
	for (int k =1; k < n; k++){
		r[k] = r[k-1]+ h;
	}
	smat(0,0) = 1.0; // left bound;
	smat(n-1, n-1) = 1.0; // right bound;
	rhs[0] = 0.0;
	rhs[n-1] = 1.0;
	for (int k = 1; k < n-1; k++){
		double rim = 1.0 / gethr(k, r), him = 1.0 / gethr(k, r);
		double rip12 = getrip12(k, r), rim12 = getrim12(k, r);
		double himr = rim * him;
		double a = himr * (rip12 / h); // r[k+1] / h2;
		double b = himr * (rim12 / h); //r[k-1] / h2;
		double c = a + b;
		smat(k, k) = c; //2 / h2;
		smat(k, k-1) = - b; //1 / h2;
		smat(k, k+1) = - a; // / h2;
	}	
};

double gethr(int i, double *r)
{
	double r0 = r[i-1], ri = r[i], r1 = r[i+1];
	double rm = (ri - r0) / 2.0;
	double rp = (r1 - ri) / 2.0;
	return rm + rp;
};

double getrip12(int i, double *r)
{
	double r1 = r[i+1], ri = r[i];
	return ri + (r1 - ri) / 2.0;
};

double getrim12(int i, double *r)
{
	double r1 = r[i-1], ri = r[i];
	return ri - (ri - r1) / 2.0;
};


void assmbSolveLapl(int n, const std::string ss, bool isPolar)
{
	AMGc::SpMat smat(n);
	double *rhs = new double[n];
	std::fill_n (rhs, n, 0.0);
	if (!isPolar) assembleMatrixDecart(smat, rhs);
	else assembleMatrixPolar(smat, rhs);
	double *result = new double[n];
	std::fill_n (result, n, 0.0);
	bool printInfo = true;
	solveWithNewAmgc(smat, rhs, result, printInfo);

	std::ostringstream oss;
	oss << ss << "/data.dat";

	std::cout << "oss = " << oss.str() << "\n";

	saveData(oss.str().c_str(), result, n);
	plotData(oss.str().c_str());

	delete[] rhs;
	delete[] result;
};

void saveData(const char *fn, double *result, int n)
{
	std::ofstream ofs(fn);
	double h = 1.0 / (n-1);
	for (int k = 0; k < n ; k++){
		ofs << h * k << " " << result[k] << "\n";
	}
	ofs.close();
};

void plotData(const char *fn)
{
	std::ostringstream oss;
	oss << "gnuplot -p -e \"plot '" << fn << "'\"";
	system(oss.str().c_str());
};
