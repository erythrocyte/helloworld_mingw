#include "mySpMat.h"

using namespace AMGc;

//---------------------------------------------------------------------------
SpMatCSR::SpMatCSR()
    : n(0), row(0), col(0), val(0), symmetric(false)
{
}

//---------------------------------------------------------------------------
SpMatCSR::SpMatCSR(int dim, int nnz, const int *I, const int *J, const AMGc::real *V)
    : n(dim), row(0), col(0), val(0), symmetric(false)
{
	row = new int [n+1];
	col = new int [nnz];
	val = new real[nnz];
	
	std::fill_n(row, n + 1, 0);
	
	for(int i = 0; i < nnz; i++) row[I[i]+1]++;
	
	for(int i = 1; i < n; i++) row[i+1] += row[i];
	
	for(int i = 0; i < nnz; i++){
		col[row[I[i]]] = J[i];
		val[row[I[i]]] = V[i];
		row[I[i]]++;
	}

	for(int i = n; i > 0; i--) row[i] = row[i-1];
	row[0] = 0;
};

//---------------------------------------------------------------------------
SpMat::SpMat(int n)
	: size(n), row(0), csr_uptodate(false), csr_row(0), csr_col(0), csr_val(0)
{
	row = new std::map<int,real>[size];
};

//---------------------------------------------------------------------------
void SpMat::clear()
{
	csr_uptodate = false;
	for(int i = 0; i < size; i++) row[i].clear();
};
//---------------------------------------------------------------------------
SpMat::~SpMat() 
{
	delete[] row;
	delete[] csr_row;
	delete[] csr_col;
	delete[] csr_val;
};

//---------------------------------------------------------------------------
int SpMat::dim() const 
{
	return size;
};

//---------------------------------------------------------------------------
int SpMat::nnz() const
{
	int n = 0;
	for(int i = 0; i < size; i++) n += (int)row[i].size();
	return n;
};

//---------------------------------------------------------------------------
AMGc::real SpMat::operator()(int i, int j) const
{
	std::map<int, real>::iterator a = row[i].find(j);
	if (a == row[i].end()) return 0;
    
	return a->second;
};

//---------------------------------------------------------------------------
AMGc::real& SpMat::operator()(int i, int j) 
{
	csr_uptodate = false;
	return row[i][j];
};

//---------------------------------------------------------------------------
std::map<int,AMGc::real>::const_iterator SpMat::begin(int i) const
{
	return row[i].begin();
};

//---------------------------------------------------------------------------
std::map<int,AMGc::real>::const_iterator SpMat::end(int i) const
{
	return row[i].end();
};

//---------------------------------------------------------------------------
SpMatCSR SpMat::csr()
{
	SpMatCSR csr;
	if (!csr_uptodate){
		delete[] csr_col;
		delete[] csr_val;
		
		if (!csr_row) csr_row = new int [size + 1];
	
		csr_col = new int [nnz()];
		csr_val = new real[nnz()];
		
		std::fill_n(csr_row, size + 1, 0);
	
		csr_row[0] = 0;

		for(int i = 0, k = 0; i < size; i++){
			for(std::map<int,real>::const_iterator a = row[i].begin(); a != row[i].end(); a++, k++){
				csr_col[k] = a->first;
				csr_val[k] = a->second;
			}
			csr_row[i+1] = k;
		}
		csr_uptodate = true;
	}
	
	SpMatCSR A;
	A.n   = size;
	A.row = csr_row;
	A.col = csr_col;
	A.val = csr_val;
	
	return A;
};
