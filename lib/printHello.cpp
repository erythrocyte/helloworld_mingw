#include "printHello.h"

void printHelloToConsole(const std::string name)
{
	std::cout << "Hello, " << name << "\n";
};
