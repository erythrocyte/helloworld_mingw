#include "solveNewAmgc.h"

#include <amgcl/make_solver.hpp>
#include <amgcl/solver/bicgstab.hpp>
#include <amgcl/amg.hpp>
#include <amgcl/coarsening/smoothed_aggregation.hpp>
#include <amgcl/relaxation/spai0.hpp>
#include <amgcl/adapter/crs_tuple.hpp>

// =============== Backends =========================
// Builtin backend: works on CPU, uses OpenMP for parallelization
#include <amgcl/backend/builtin.hpp>
// ==================================================

// =============== Matrix adapters ==================
// Allows to specify system matrix as a tuple of sizes and ranges (as in Boost.Range).
#include <amgcl/adapter/crs_tuple.hpp>
// 
#include <amgcl/adapter/crs_builder.hpp>
// ==================================================

// =============== Coarsening strategies ============

// Classic Ruge-Stuben coarsening algorithm
#include <amgcl/coarsening/ruge_stuben.hpp>
// ********** Aggregation based coarsening strategies *************
// non smoothed aggregation
#include <amgcl/coarsening/aggregation.hpp>
// smoothed aggregation
#include <amgcl/coarsening/smoothed_aggregation.hpp>
// smoothed aggregation with energy minimization
#include <amgcl/coarsening/smoothed_aggr_emin.hpp>
// ****************************************************************
// ==================================================


// ======== Relaxation schemes=======================
// Damped Jacobi relaxation
#include <amgcl/relaxation/damped_jacobi.hpp>
// Gauss-Seidel relaxation
#include <amgcl/relaxation/gauss_seidel.hpp>
// ILU0 smoother
#include <amgcl/relaxation/ilu0.hpp>
// Sparse approximate inverse smoother
#include <amgcl/relaxation/spai0.hpp>
// Chebyshev polynomail smoother
#include <amgcl/relaxation/chebyshev.hpp>
// ==================================================


// ================ Solvers =========================
// BiCGStab iterative solver
#include <amgcl/solver/bicgstab.hpp>
// BiCGStab(L)
#include <amgcl/solver/bicgstabl.hpp>
// Conjugate Gradient solver
#include <amgcl/solver/cg.hpp>
// GMRES
#include <amgcl/solver/gmres.hpp>
// ==================================================


void solveWithNewAmgc(AMGc::SpMat &smat, double *rhs, double *result, bool printInfo)
{
	time_t startTime; time (&startTime);

	int msize = smat.dim();
	if (printInfo){
		std::cout << "NONZEROVAL = " << smat.nnz() << std::endl;
		std::cout << "MSIZE = " << msize << std::endl;
	}
	AMGc::SpMatCSR mcsr; mcsr = smat.csr();
	//Define the AMG type:
	typedef amgcl::amg<
		amgcl::backend::builtin<double>,
		amgcl::coarsening::ruge_stuben,
		amgcl::relaxation::gauss_seidel
	> AMG;

		//amgcl::coarsening::smoothed_aggregation,
		//amgcl::coarsening::ruge_stuben,
		//amgcl::coarsening::smoothed_aggr_emin,
		//amgcl::coarsening::aggregation,
		//amgcl::coarsening::ruge_stuben,
		//amgcl::relaxation::gauss_seidel
	//> AMG;

		//amgcl::coarsening::ruge_stuben,
		//amgcl::coarsening::aggregation,
		//amgcl::coarsening::smoothed_aggr_emin,


		//amgcl::relaxation::gauss_seidel
		//amgcl::relaxation::damped_jacobi
		//amgcl::relaxation::ilu0
		//amgcl::relaxation::spai0
		//amgcl::relaxation::chebyshev
		

	//std::cout << "Using " << amgcl::relaxation::chebyshev << " as preconditioner" << std::endl;
	// Construct the AMG hierarchy.
	// Note that this step only depends on the matrix. Hence, the constructed
	// instance may be reused for several right-hand sides.
	// The matrix is specified as a tuple of sizes and ranges.
	//
	//
	//AMG::params prm;
	//prm.coarsening.aggr.eps_strong = 1e-14;
	//prm.backend.tol = 1e-14;;
	
	AMG amg(
		boost::make_tuple(
			msize,
			boost::make_iterator_range(mcsr.row, mcsr.row + msize + 1),
			boost::make_iterator_range(mcsr.col, mcsr.col + mcsr.row[msize]),
			boost::make_iterator_range(mcsr.val, mcsr.val + mcsr.row[msize])
		)
		//,
		//prm
	);

	//Use BiCGStab as an iterative solver:
	typedef amgcl::solver::gmres<
		amgcl::backend::builtin<double>
		> Solver;

	//typedef amgcl::solver::bicgstab<
	//typedef amgcl::solver::bicgstabl<
	//typedef amgcl::solver::cg<
	//typedef amgcl::solver::gmres<

	// Construct the iterative solver. It needs size of the system to
	// preallocate the required temporary structures:
	Solver::params prm2;
	prm2.tol = 1e-10;
	prm2.maxiter = 500;
	Solver solve(msize, prm2);

	
	// The solution vector. Use zero as initial approximation.
	std::vector<double> x(msize, 0.0);
	std::fill_n (result, msize, 0.0);
	
	// Solve the system. Returns number of iterations made and the achieved residual.
	int iters; 
	double resid; 
	std::vector<double> rhs2;
	for (int k = 0; k < msize; ++k) rhs2.push_back(rhs[k]);
	boost::tie(iters, resid) = solve(amg, rhs2, x);

	if (printInfo){
		std::cout << "Iterations: " << iters << std::endl
			<< "Error:      " << resid << std::endl
			<< std::endl;
		//std::cout << "M = " << m << std::endl; 
	}
	else{
		//std::cout << "Iterations: " << iters << std::endl;
		//std::cout << "Error:      " << resid << std::endl;
	}
	for (int k = 0; k < msize; ++k) result[k] = x[k];

	time_t endTime; time(&endTime);

	double diffTime = difftime(endTime, startTime);
	//save for relaxation
	//std::ofstream ofs("coarseSmoothedBiGCStab.txt", std::ios_base::app);
	//std::ofstream ofs("gaussSeidelRsGmres.txt", std::ios_base::app);
	//ofs << m << " " << iters << " " << resid << " " << diffTime << "\n";
	//ofs.close();
};
