#ifndef SOLVELAPLACE_H
#define SOLVELAPLACE_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>

//custom
#include "solveNewAmgc.h"
#include "mySpMat.h"

void assmbSolveLapl(int n, const std::string ss, bool isPolar);
void assembleMatrix(AMGc::SpMat &smat, double *rhs);
void assembleMatrixPolar(AMGc::SpMat &smat, double *rhs);
void saveData(const char *fn, double *result, int n);
void plotData(const char *fn);
double gethr(int i, double *r);
double getrip12(int i, double *r);
double getrim12(int i, double *r);

#endif
