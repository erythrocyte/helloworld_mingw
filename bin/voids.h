#ifndef VOIDS_H
#define VOIDS_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

//custom
#include "printHello.h"
#include "solveLaplace.h"

void printMessage (const std::string &name);
void testLaplEq(const std::string ss, bool isPolar);

#endif
