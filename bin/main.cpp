#include <iostream>
#include <sstream>
#include <fstream>

//custom
#include "voids.h"

int main(int argc, char* argv[])
{
	std::cout << "insert your name here : ";
	std::string nm = "";
	std::cin >> nm;
	printMessage(nm);
	std::cout << argc << "\n";
	for (int k = 0; k < argc; k++)
		std::cout << argv[k] << "\n";
	std::ostringstream oss;
	std::string pathMy = "";
	bool isPolar = true;
	if (argc > 1){
		oss << argv[1]; // path to file and where to save
		pathMy = oss.str();
		std::cout << "pm = " << pathMy << "\n";
	}
	if (argc > 2){
		oss.str("");
		oss << argv[2];
		std::string mm= oss.str();
		if (mm == "polar") isPolar = true;
		else isPolar = false;
	}
	testLaplEq(pathMy, isPolar);
};
